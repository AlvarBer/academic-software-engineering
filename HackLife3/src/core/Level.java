package core;

import java.util.Scanner;

public class Level {
	private int levelPoints;
	private String statement;
	private String answer;
	private String inboxMsg;
	private String winMsg;
	private boolean won;
	 
	public Level (int levelPoints, String statement, String answer, String inboxMsg, boolean won) {
		this.levelPoints = levelPoints;
		this.statement = statement;
		this.answer = answer;
		this.inboxMsg = inboxMsg;
		this.won = won;
	}
	
	// Run level: Executes the puzzle
	// input from the input the answer, checks the answer
	// Exit the loop if won. Continues in other case
	// => If won, add the points
	
	public void runLevel(Scanner in) {
		String userAnswer;
		System.out.println(statement);
		System.out.println("Insert your answer: ");
		userAnswer = in.nextLine();
		while (!checkAnswer(userAnswer)) {
			System.out.println("Wrong Answer");
			System.out.println(statement);
			System.err.println("Right answer :" + this.getAnswer());
			System.out.println("insert your answer: ");
			userAnswer = in.nextLine();
		}
	}

	// Checks the inputs from the user and compares with the answer
	
	public boolean checkAnswer( String userAnswer) {
		boolean valid = false;
		if (this.answer.equals(userAnswer)) {
			valid = true;
		}
		return valid;
	}
	
	// Increase the total points of the level
	// taking in account the levelPoints. ANd in the future una penalizaciï¿½n
	// en caso de que se hayan usado pistas.
	//
	//	public void addPoints( int points ) {
	//		levelPoints += points;
	//	}

	// Getters and setters for some atributes
	
	public int getLevelPoints() {
		return levelPoints;
	}
 
	public String getStatement() {
		return statement;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getInboxMsg() {
		return inboxMsg;
	}

	public void setInboxMsg(String inboxMsg) {
		this.inboxMsg = inboxMsg;
	}

	public boolean isWon() {
		return won;
	}

	public void setWon(boolean won) {
		this.won = won;
	}
	
	public String getWinMsg() {
		return winMsg;
	}

	public void setWinMsg(String winMsg) {
		this.winMsg = winMsg;
	}
}