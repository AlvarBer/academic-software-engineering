package core;

import other.FileManager;
import java.util.Scanner;
public class Mission {

	private String IP;
	private String missionInbox;
	private String missionStatement;
	private String missionName;
	private int totalLevels;
	private int missionPoints;
	private boolean wonMission;
	private Level[] missionLevels; // Array of levels.
	private boolean missionLoaded;
	 
	// Constructor
	public Mission(String missionName) {
		this.missionName = missionName;
	}
	
	// Load mission from XML given a name
	// return false if there was an error.
	public void  loadMission() {
		FileManager.loadMission(this);
	}

	// Run Mission:
	// Shows the Mission Statements
	// Execute the loop until all the levels have finishe
	// Add the points when each mission has finished.
	// Cuando termine la misión, volverá al menú principal
	
	public void runMission(Scanner in) {
		int counter = 0;
		System.out.println(missionStatement);

		do {
			missionLevels[counter].runLevel(in);
			missionPoints += missionLevels[counter].getLevelPoints();
		} while (counter < totalLevels);
	}

	// Getters and setters

	public String getIP() {
		return IP;
	}

	public String getMissionInbox() {
		return missionInbox;
	}

	public String getMissionStatement() {
		return missionStatement;
	}

	public int getTotalLevels() {
		return totalLevels;
	}

	public int getMissionPoints() {
		return missionPoints;
	}

	public boolean isWonMission() {
		return wonMission;
	}

	public Level[] getMissionLevels() {
		return missionLevels;
	}

	public void setIP(String iP) {
		IP = iP;
	}

	public void setMissionInbox(String missionInbox) {
		this.missionInbox = missionInbox;
	}

	public void setMissionStatement(String missionStatement) {
		this.missionStatement = missionStatement;
	}

	public void setTotalLevels(int totalLevels) {
		this.totalLevels = totalLevels;
	}

	public void setMissionPoints(int missionPoints) {
		this.missionPoints = missionPoints;
	}

	public void setWonMission(boolean wonMission) {
		this.wonMission = wonMission;
	}

	public void setMissionLevels(Level[] missionLevels) {
		this.missionLevels = missionLevels;
	}
	 
	public String getMissionName() {
		return this.missionName;
	}
	
	public void setMissionName( String name ) {
		this.missionName = name;
	}
	
	public void setMissionLoaded(boolean missionLoaded) {
		this.missionLoaded = missionLoaded;
	}
	
	public boolean getMissionLoaded() {
		return this.missionLoaded;
	}
	
}