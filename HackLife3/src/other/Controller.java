package other;

import org.xml.sax.SAXException;
import users.User;
import users.UserList;
import core.Mission;
import java.io.IOException;
import java.util.Scanner;
import other.Resources;

import javax.xml.parsers.ParserConfigurationException;

public class Controller {
	private UserList userList; // List of users
	private Mission currentMission;
	private Scanner in;
	
	public Controller() {
		this.in = new Scanner(System.in);
		try {
			userList = new UserList();
		}
		catch (ParserConfigurationException ex) {
			ex.printStackTrace();
		}
		catch (SAXException ex) {
			ex.printStackTrace();
		}
		catch(IOException ex) {
			ex.printStackTrace();
		}
	}

	public void play() {
		String userName;
		User currentUser;
		int option;
		String currentMissionName;

		// Ask for username
		System.out.println("€NT3R YØU® NÅM£");
		userName = in.nextLine();

		// See if users exists
		if (userList.existUser(userName)) {
			System.out.println("\nW3LCØMË ÅGÃ1N\n");
			currentUser = userList.getUserFromList(userName);
		}
		// User doesn't exists, we create a new one
		else {
			System.out.println("\nWÉ¬ÇºM€ N00B\n");
			currentUser = new User(userName, 0, "Mission", false);
			userList.appendUser(currentUser);
			try {
				userList.saveUsers();
				userList = new UserList();
			} catch (ParserConfigurationException ex) {
				ex.printStackTrace();
			} catch (SAXException ex) {
				ex.printStackTrace();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		currentMissionName = currentUser.getCurrentMission(); // Get mission name from user
		currentMission = new Mission(currentMissionName);
		currentMission.loadMission();

		// Shows the menu: Run mission, open Inbox, show help, see credits and exit the game.
		do {
			option = Resources.displayMenu(in);
			if (option == 1) {
				boolean exit = false;
				String userCommand;

				do {
					//Print initial inbox message
					System.out.println(currentMission.getMissionInbox());
					System.out.println("Please introduce the IP of the server (Write \"connect \" + IP) or 0 to exit");
					userCommand = in.nextLine();
					if (userCommand.equals("connect " + (currentMission.getIP()))) {
						currentMission.runMission(in);
						// Save mission points
						//this.loadNextMission();
						exit = true;
					}
					else if (userCommand == "0") {
						exit = true;
					}
				} while (!exit);
			}
			else if (option == 2) { // If this option is chosen run the mission directly without showing the inbox message
				currentMission.runMission(in);
				// Save mission points
				this.loadNextMission();
			}
			else if (option == 3) {
				Resources.displayHelp();
				System.out.println("Press enter to return to the main menu");
				in.nextLine();
			}
			else if (option == 4) {
				Resources.displayCredits();
				System.out.println("Press enter to return to the main menu");
				in.nextLine();
			}
		} while (option != 0);

		System.out.println("Logging off");
		try {
			userList.saveUsers();
		}
		catch (java.io.IOException ex) {
			ex.printStackTrace();
		}
	}

	private void loadNextMission() {
		//Should load next mission
	}
}