package other;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import users.User;
import users.UserList;
import core.Level;
import core.Mission;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;

public class FileManager {
	private static final int SERVER_LEVEL_NUMBER = 1;
	private static final int LEVELS_PER_MISSION = 2;
	private static final String USER_LIST_FILENAME = "./res/users.xml";
	
	/*
	public static void main(String[] arg0) throws IOException {
		@SuppressWarnings("unused")
		UserList list = new UserList();
		try {
			list = FileManager.loadUserList(list);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/

	public static Mission loadMission (Mission mission) {
		
		String name = mission.getMissionName(); 
		
		try {
			File fXmlFile = new File("./res/"+name+".xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
		 
			doc.getDocumentElement().normalize();
			
			Node nNode;
			NodeList nList;
			Element eElement;
			
			//Load levels
		 
			 nList = doc.getElementsByTagName("level");
			 Level levelList[] = new Level[FileManager.LEVELS_PER_MISSION];
			 int levelCount = 0;
		 
			for (int temp = 0; temp < FileManager.LEVELS_PER_MISSION; temp++) {
				if (temp != FileManager.SERVER_LEVEL_NUMBER){
					 nNode = nList.item(temp);
			 
					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						eElement = (Element) nNode;
						Level puzzle = new Level(0, null, null, null, false);
						puzzle.setStatement(removeTabsFromString(eElement.getElementsByTagName("statement").item(0).getTextContent()));
						puzzle.setAnswer(removeTabsFromString(eElement.getElementsByTagName("answer").item(0).getTextContent()));
						//puzzle.addPoints(Integer.parseInt(eElement.getElementsByTagName("points-earned").item(0).getTextContent()));
						puzzle.setWinMsg(removeTabsFromString((eElement.getElementsByTagName("complete-message").item(0).getTextContent())));
						levelList[levelCount] = puzzle;
						levelCount++;
					}
				
				} else {
					//Load server level
					Level serverLevel = FileManager.loadServerLevel();
					levelList[levelCount] = serverLevel;
					levelCount++;					
				}
			}
			//Set mission levels
			mission.setMissionLevels(levelList);
			
			//Load IP
			eElement = (Element) doc.getElementsByTagName("IP").item(0);
			mission.setIP(eElement.getTextContent());
			
			//Load Mission Inbox
			
			eElement = (Element) doc.getElementsByTagName("mission-inbox").item(0);
			mission.setMissionInbox(removeTabsFromString(eElement.getTextContent()));
			
			//Load Mission Inbox
			
			eElement = (Element) doc.getElementsByTagName("mission-statement").item(0);
			mission.setMissionStatement(removeTabsFromString(eElement.getTextContent()));
			
		 } 
		catch (Exception e) {
			e.printStackTrace();
			//mission.setLoaded(false);
		}		
	return mission;
	}

	public static String[] loadServerList() {
		String [] serverList = null;
		try {
			 
			File fXmlFile = new File("./res/serverList.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
		 
			doc.getDocumentElement().normalize();
			
			Node nNode;
			NodeList nList;
			Element eElement;
		 
			
			//Load levels
		 
			 nList = doc.getElementsByTagName("serv");
			 serverList = new String[nList.getLength()];
		 
			for (int temp = 0; temp < nList.getLength(); temp++) {
		 
				 nNode = nList.item(temp);
		 
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					eElement = (Element) nNode;
					serverList[temp] = eElement.getTextContent();
				}
			}
				
		 } 
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return serverList;
	}
	
	public static String[] ShuffleArray(String[] array){
		int index;
		String temp;
	    Random random = new Random();
	    for (int i = array.length - 1; i > 0; i--)
	    {
	        index = random.nextInt(i + 1);
	        temp = array[index];
	        array[index] = array[i];
	        array[i] = temp;
	    }
	    return array;
	}	
	
	public static Level loadServerLevel() {
		// TODO Auto-generated method stub
		Level puzzle = new Level(0, null, null, null, false);
		String statement;
		String answer;
		String[] serverList = FileManager.loadServerList();
		FileManager.ShuffleArray(serverList);
		int port = 0000, ramPort;
		Random random  = new Random();
		statement = "Intranet SharePoint NPI Serverlist:\n\n";
		statement += "Name                      " + "Port\n"; //22 empty spaces + 4 letters
		for (int i = 0; i < serverList.length; i++) {
			statement += serverList[i]; 
			for (int j = 0; j < 26 - serverList[i].length(); j++)
				statement += " ";
			ramPort = random.nextInt(10000);
			if(serverList[i] == "main.serv") 
				port = ramPort;
			statement += ramPort + "\n"; 		
		}
		answer = "connect -intra main.serv -port " + Integer.toString(port);
		puzzle.setStatement(statement);
		puzzle.setWinMsg("Correct!");
		//puzzle.addPoints(200);
		puzzle.setAnswer(answer);
		
		return puzzle;
	}
	
	public static void storeUserList(UserList list) throws IOException {
		
		FileOutputStream writeStream = new FileOutputStream(FileManager.USER_LIST_FILENAME);
		
		writeStream.write("<user-list>".getBytes());
		
		for(int i = 0; i < list.getTotalUsers(); i++) {
			User user = list.getUserFromList(i);
			FileManager.storeUser(user, writeStream);
		}
		
		writeStream.write("</user-list>".getBytes());
		
		writeStream.close();
	}

	private static void storeUser(User user, FileOutputStream writeStream) throws IOException {
		// TODO Auto-generated method stub
		writeStream.write("<user>".getBytes());
		/*
		private String name;
		private int points;
		private String currentMission; // Si es un nuevo usuario, carga la primera misi�n
		private boolean oldUser; // Checks if the points and the mission is greater than 1.
		*/
		String field = new String();
		//Store the username
		field = "<username>" + user.getName() + "</username>\n";
		writeStream.write(field.getBytes());
		//Store the points
		field = "<points>" + user.getPoints() + "</points>\n";
		writeStream.write(field.getBytes());
		//Store Current Mission
		field = "<current-mission>" + user.getCurrentMission() + "</current-mission>";
		writeStream.write(field.getBytes());
		//Store oldUser
		field = "<old-user>" + user.isOldUser() + "</old-user>";
		writeStream.write(field.getBytes());
		
		writeStream.write("</user>".getBytes());
		
	}
	
	public static UserList loadUserList(UserList userList) throws ParserConfigurationException, SAXException, IOException {
		
		File fXmlFile = new File("./res/users.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(fXmlFile);
	 
		doc.getDocumentElement().normalize();
		
		Node nNode;
		NodeList nList;
		Element eElement;
		
		nList = doc.getElementsByTagName("user");
		User [] users = new User[nList.getLength() + 1];
		int total = 0;
		
		for(int temp = 0; temp < nList.getLength(); temp++) {
			
			nNode = nList.item(temp);
			 
			eElement = (Element) nNode;
			User user = FileManager.loadUser(eElement);
			users[temp] = user;
			total++;
		}	
		
		
		userList.setUserList(users);
		userList.setTotalUsers(total);
		return userList;
	}

	private static User loadUser(Element eElement) {
		// TODO Auto-generated method stub
		
		String name;
		int points;
		String currentMission; // Si es un nuevo usuario, carga la primera misi�n
		boolean oldUser; // Checks if the points and the mission is greater than 1.
		
		name = eElement.getElementsByTagName("username").item(0).getTextContent();
		points = Integer.parseInt(eElement.getElementsByTagName("points").item(0).getTextContent());
		currentMission = eElement.getElementsByTagName("current-mission").item(0).getTextContent();
		oldUser = Boolean.parseBoolean(eElement.getElementsByTagName("old-user").item(0).getTextContent());
		
		User user = new User(name, points, currentMission, oldUser);
		
		return user;
	}

	public static String removeTabsFromString(String str) {
		return str = str.replace("\t","");
	}
}
