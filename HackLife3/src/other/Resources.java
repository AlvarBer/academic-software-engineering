package other;

import java.util.Scanner;
 
public class Resources {
	final static int MAX_USERS = 10;

	// MENU: Shows the menu: Run mission, open Inbox, show help, see credits and exit the game.
	public static int displayMenu(Scanner in) {
		int option;
		boolean valid;
		System.out.println("888    888        d8888  .d8888b.  888    d8P        888      8888888 8888888888 .d8888b.  \n" +
				"888    888       d88888 d88P  Y88b 888   d8P         888        888   888       d88P  Y88b \n" +
				"888    888      d88P888 888    888 888  d8P          888        888   888            .d88P \n" +
				"8888888888     d88P 888 888        888d88K           888        888   8888888       8888\"  \n" +
				"888    888    d88P  888 888        8888888b          888        888   888            \"Y8b. \n" +
				"888    888   d88P   888 888    888 888  Y88b         888        888   888       888    888 \n" +
				"888    888  d8888888888 Y88b  d88P 888   Y88b        888        888   888       Y88b  d88P \n" +
				"888    888 d88P     888  \"Y8888P\"  888    Y88b       88888888 8888888 888        \"Y8888P\"\n  ");
		System.out.println("1. Open Inbox");
		System.out.println("2. Start mission");
		System.out.println("3. Check Help");
		System.out.println("4. See credits");
		System.out.println("0. Quit");
		do {
			System.out.print("\nSelect an option: ");
			option = in.nextInt();
			in.nextLine();
			valid = option >= 0 && option <= 4;
			if (!valid)
				System.out.println("Invalid option. Please, try again");
		} while (!valid);
		return option;
	}

	// Show the help of the program
	public static void displayHelp() {
		System.out.println("\nHow to win Hack Life 3:\n1. Read the mission statement in the Inbox carefully\n2. Start the mission and try to solve the puzzles"
				+ "\n3. If you get stuck or don't know the answer, you can ask for a hint (With a penalty in the final score)\n");
	}

	// Show the credits of the program
	public static void displayCredits() {
		System.out.println("——————————————————————————————————————————————");
		System.out.print("  ,ad8888ba,                                    88  88                      \n" +
				" d8\"'    `\"8b                                   88  \"\"    ,d                \n" +
				"d8'                                             88        88                \n" +
				"88             8b,dPPYba,   ,adPPYba,   ,adPPYb,88  88  MM88MMM  ,adPPYba,  \n" +
				"88             88P'   \"Y8  a8P_____88  a8\"    `Y88  88    88     I8[    \"\"  \n" +
				"Y8,            88          8PP\"\"\"\"\"\"\"  8b       88  88    88      `\"Y8ba,   \n" +
				" Y8a.    .a8P  88          \"8b,   ,aa  \"8a,   ,d88  88    88,    aa    ]8I  \n" +
				"  `\"Y8888Y\"'   88           `\"Ybbd8\"'   `\"8bbdP\"Y8  88    \"Y888  `\"YbbdP\"'  \n");
		System.out.println("——————————————————————————————————————————————");
		System.out.println("\nProject developed by iTeam:\n\nAlvaro Bermejo\nPedro García\nJorge García\nBorja Lorente\nFrancisco Lozano\n");
	}
}