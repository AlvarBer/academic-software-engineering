package users;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import other.FileManager;

public class UserList {
	private User[] users;
	private int totalUsers;
	 	
	public UserList() throws ParserConfigurationException, SAXException, IOException{
		loadUserList();
	}
	
	// XML: Load the users list from the XML.
	// Pass the array to the XML and it returns the array of users filled.
	// It has to call the function of Load User on "User class" 

	public void loadUserList() throws ParserConfigurationException, SAXException, IOException {
		FileManager.loadUserList(this);
	}
	
	// XML: When exit the game. Save the users

	public void saveUsers() throws IOException {
		FileManager.storeUserList(this);
	}

	// Checks in the user exist in the list of users

	public boolean existUser( String name ) {
		boolean exist = false;
		for (int i = 0; i < totalUsers; i++){
			if (users[i].getName() == name){
				exist = true;
			}
		}
		return exist;	
	}	
	
	// Given a name (inputeado por el usuario).
	// Devuelve el usuario cargado de la lista
	
	public User getUserFromList( String name ) {// a esta funcion le falta el como devolver si el usuario no se encuentra.
		boolean found = false;
		int counter = 0;
		
		while (!found && (counter < totalUsers)){
			if (users[counter].getName() == name){
				found = true;
			}
			else {
				counter++;
			}
		}
		
		return users[counter];
	}
	
	//Overload de la funcion anterior, para poder usar un index int
	public User getUserFromList( int index) {
		return this.users[index];
	}
	
	// Getters and setters
	
	public int getTotalUsers() {
		return this.totalUsers;
	}
	 
	// Devuelve un objeto del tipo UserList
	
	public UserList getUserList() {
		return this;
	}
	
	//append user
	public void appendUser(User user){
		users[totalUsers] = user;
	}
	
	public void setUserList(User[] users) {
			this.users = users;
	}
	
	public void setTotalUsers(int totalUsers) {
		this.totalUsers = totalUsers;
	}
}
